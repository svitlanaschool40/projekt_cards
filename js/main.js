function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

function showFields() {
  let select = document.getElementById("doctorSelect");
  let selectedDoctor = select.options[select.selectedIndex].value;
  let additionalFields = document.getElementById("additionalFields");

  if (selectedDoctor === "cardiologist") {
    additionalFields.innerHTML = `
      <input type="number" placeholder="Звичайний тиск" required>
      <input type="number" placeholder="Індекс маси тіла" required>
      <input type="text" placeholder="Перенесені захворювання серцево-судинної системи" required>
      <input type="number" placeholder="Вік" required>
    `;
  } else if (selectedDoctor === "dentist") {
    additionalFields.innerHTML = `
      <input type="date" placeholder="Дата останнього відвідування стоматолога" required>
    `;
  } else if (selectedDoctor === "therapist") {
    additionalFields.innerHTML = `
      <input type="number" placeholder="Вік" required>
    `;
  } else {
    additionalFields.innerHTML = "";
  }

  additionalFields.style.display = "block";
}

function saveVisit() {
  let select = document.getElementById("doctorSelect");
  let selectedDoctor = select.options[select.selectedIndex].value;
  let fields = document.getElementById("additionalFields").getElementsByTagName("input");
  let commentsField = document.querySelector('#myModal input[type="text"]');

  let visitData = {
    doctor: selectedDoctor,
    visitPurpose: fields[0].value,
    visitDescription: fields[1].value,
    urgency: fields[2].value,
    fullName: fields[3].value,
    comments: commentsField.value
  };

  if (selectedDoctor === "cardiolog") {
    visitData.regularPressure = fields[4].value;
    visitData.bodyMassIndex = fields[5].value;
    visitData.cardiovascularHistory = fields[6].value;
    visitData.age = fields[7].value;
  } else if (selectedDoctor === "dentist") {
    visitData.lastDentistVisit = fields[4].value;
  } else if (selectedDoctor === "therapevt") {
    visitData.age = fields[4].value;
  }

let token = "85f6b4c2-cf71-4655-bc37-2911d10e98d6"; 
fetch("https://ajax.test-danit.com/api/v2/cards", {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  },
  
})
  .then(response => response.json())
  .then(response => console.log(response))
}